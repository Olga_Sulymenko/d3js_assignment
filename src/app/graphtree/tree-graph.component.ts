import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { MatDialog  } from '@angular/material/dialog';

import * as d3 from 'd3';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
    selector: 'app-tree-graph',
    templateUrl: './tree-graph.component.html',
    styleUrls: ['./tree-graph.component.css']
})
export class TreeGraphComponent implements OnInit, AfterViewInit, OnDestroy {
  name: string;
  svg;
  link;
  node;
  constructor(
    public dialog: MatDialog
  ) {}
  ngOnInit() {
  }
  ngAfterViewInit() {
    this.svg = d3.select('svg');

    // Set the dataset
    const treeData = {
      'name': 'Протеи',
      'children': [
        {'name': 'Археи',
          'children': []},
        {
          'name': 'Простейшие',
          'children': [
            {'name': 'Растения',
              'children': []
            },
            {'name': 'Водоросли',
              'children': [] }
            ,
            {
              'name': 'Кишечнополостные',
              'children': [
                {'name': 'Черви',
                'children': [
                  {'name': 'Хордовые',
                    'children': [
                      {'name': 'Рыбы',
                        'children': [
                          {'name': 'Млекопитающие',
                            'children': []},
                          {'name': 'Птицы',
                            'children': []},
                          {'name': 'Амфибии',
                            'children': []}
                        ]}
                    ]}
                ]},
                {'name': 'Моллюски',
                  'children': []},
                {'name': 'Членистоногие',
                  'children': []}]
            },
            {'name': 'Слизневики',
              'children': [] }
            ,
            {'name': 'Грибы',
              'children': [] }
          ]
        },
        {'name': 'Бактерии',
          'children': []}
      ]
    };

    // Set the dimensions and margins of the diagram
    const margin = {top: 20, right: 90, bottom: 30, left: 90},
      width = 1800 - margin.left - margin.right,
      height = 700 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    // appends a 'group' element to 'svg'
    // moves the 'group' element to the top left margin
    const svg = d3.select('body').append('svg')
      .attr('width', width + margin.right + margin.left)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate('
        + margin.left + ',' + margin.top + ')');

    let i = 0,
      root;
    const duration = 750;

    // declares a tree layout and assigns the size
    const treemap = d3.tree().size([height, width]);

    // Assigns parent, children, height, depth
    root = d3.hierarchy(treeData, d => d.children);
    root.x0 = height / 2;
    root.y0 = 0;

    update(root, this);



    function update(source, api) {
      // Assigns the x and y position for the nodes
      const treeDataV = treemap(root);
      // Compute the new tree layout.
      const nodes = treeDataV.descendants(),
        links = treeDataV.descendants().slice(1);
      // Normalize for fixed-depth.
      nodes.forEach(function (d) {
        d.y = d.depth * 180;
      });
      // Update the nodes
      const node = svg.selectAll('g.node')
        .data(nodes, function (d) {
          return d.id || (d.id = ++i);
        });

      const nodeEnter = node.enter().append('g')
        .attr('class', 'node')
        .attr('transform', function (d) {
              return 'translate(' + source.y0 + ',' + source.x0 + ')';
        })
        .on('click', click);
      nodeEnter.append('circle')
        .attr('class', 'node')
        .attr('r', 1e-6)
        .style('fill',  '#fff');

      // Add labels for the nodes
      nodeEnter.append('text')
        .attr('dy', '.35em')
        .attr('y', function (d) {
          return -20;
        })
        .attr('x', function (d) {
          return  -d.data.name.length * 3.14;  // some magic with centring
        })
        .attr('text-anchor', function (d) {
          return 'start';
        })
        .text(function (d) {
          return d.data.name;
        });


      const nodeUpdate = nodeEnter.merge(node);


      nodeUpdate.transition()
        .duration(duration)
        .attr('y', function (d) {
          return d.children || d._children ? -18: 0;
        })
        .attr('transform', function (d) {
          return 'translate(' + d.y + ',' + d.x + ')';
        });


      nodeUpdate.select('circle.node')
        .attr('r', 10)
        .style('fill', function (d) {
          return ((d.depth === 9) || (d.height === 10)) ? 'lightsteelblue' : '#fff';
        })
        .style('stroke-dasharray', function (d) {
          return ((d.depth === 9) || (d.height === 10)) ? '4'  : NaN;
        })
        .attr('cursor', 'pointer');



      const link = svg.selectAll('path.link')
        .data(links, function (d) {
          return d.id;
        });

      const linkEnter = link.enter().insert('path', 'g')
        .attr('class', 'link')
        .attr('d', function (d) {
          let s;
          let de;
          {
            s = {x: source.x0, y: source.y0};
            de = {x: source.x0, y: source.y0};
          }
          return diagonal(s, de);
        });


      const linkUpdate = linkEnter.merge(link);


      linkUpdate.transition()
        .duration(duration)
        .attr('d', function (d) {
          return diagonal(d, d.parent);
        });



      nodes.forEach(function (d) {
        d.x0 = d.x;
        d.y0 = d.y;
      });

      // Creates a curved (diagonal) path from parent to the child nodes
      function diagonal(s, d) {
        return `M ${s.y} ${s.x}
            C ${(s.y + d.y) / 2} ${s.x},
              ${(s.y + d.y) / 2} ${d.x},
              ${d.y} ${d.x}`;
      }

      function click(d) {
        if ((d.depth < 9) && (d.height < 10)) {
          api.openDialog(d, function (new_node_name) {
            console.log('new_node_request', d, new_node_name);
            if (new_node_name.length > 0){
              const tmpNode = {
                type: 'node-type',
                name: new_node_name,
                children: []
              };
              if (!d.children){
                d.children = [];
              }
              let newNode = d3.hierarchy(tmpNode);
              newNode.depth = d.depth + 1 ;
              newNode.height = 0 ;
              newNode.parent = d;
              d.height += 1;
              d.children.push(newNode);
              d.data.children.push(newNode.data);
              update(d, api);
            }
          });
        }
      }
    }
  }

  openDialog(node, on_result): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {name: this.name}
    });

    dialogRef.afterClosed().subscribe(result => {
      on_result(result);
      console.log('The dialog was closed');
      node.data.name = result;
      console.log(this.name); });
  }
  ngOnDestroy() {

  }

}

